package ru.t1.schetinin.tm.api.service;

import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
