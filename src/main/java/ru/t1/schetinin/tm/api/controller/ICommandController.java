package ru.t1.schetinin.tm.api.controller;

public interface ICommandController {

    void showErrorArgument();

    void showSystemInfo();

    void showErrorCommand();

    void showVersion();

    void showAbout();

    void showHelp();

}
